# Utilisez ce Dockerfile pour répondre à l'exercice
FROM node:6-alpine
RUN apk add --no-cache tini
WORKDIR /usr/src/app
EXPOSE 3000/tcp
COPY package.json .
RUN npm install && npm cache clean --force
COPY . /usr/src/app/
CMD ["/sbin/tini", "--", "node", "./bin/www"]
